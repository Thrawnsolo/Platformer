import phaser from 'phaser';
import Enemy from '../prefabs/enemy';

export default class Game extends phaser.State {
  init(level) {
    this.currentLevel = level || 'level1';
    this.RUNNING_SPEED = 180;
    this.JUMPING_SPEED = 500;
    this.BOUNCING_SPEED = 150;

    this.game.physics.arcade.gravity.y = 1000;
    this.cursors = this.game.input.keyboard.createCursorKeys();
  }

  create() {
    this.loadLevel();
    this.createOnScreenControls();
  }

  static changeLevel(player, goal) {
    this.game.state.start('Game', true, false, goal.nextLevel);
  }

  hitEnemy(playerObject, enemy) {
    const player = playerObject;
    if (enemy.body.touching.up) {
      enemy.kill();
      player.body.velocity.y = -this.BOUNCING_SPEED;
    } else {
      this.gameOver();
    }
  }

  gameOver() {
    this.game.state.start('Game', true, false, this.currentLevel);
  }

  update() {
    this.game.physics.arcade.collide(this.player, this.collisionLayer);
    this.game.physics.arcade.collide(this.enemies, this.collisionLayer);
    this.game.physics.arcade.overlap(this.player, this.goal, Game.changeLevel, null, this);
    this.player.body.velocity.x = 0;

    this.game.physics.arcade.collide(this.player, this.enemies, this.hitEnemy, null, this);

    if (this.cursors.left.isDown || this.player.customParams.isMovingLeft) {
      this.player.body.velocity.x = -this.RUNNING_SPEED;
      this.player.scale.setTo(1, 1);
      this.player.play('walking');
    } else if (this.cursors.right.isDown || this.player.customParams.isMovingRight) {
      this.player.body.velocity.x = this.RUNNING_SPEED;
      this.player.scale.setTo(-1, 1);
      this.player.play('walking');
    } else {
      this.player.animations.stop();
      this.player.frame = 3;
    }

    if ((this.cursors.up.isDown || this.player.customParams.mustJump) &&
      (this.player.body.blocked.down || this.player.body.touching.down)) {
      this.player.body.velocity.y = -this.JUMPING_SPEED;
      this.player.customParams.mustJump = false;
    }

    if (this.player.bottom === this.game.world.height) {
      this.gameOver();
    }
  }

  static findObjectByType(type, tileMap, layerName) {
    const result = [];
    tileMap.objects[layerName].forEach((e) => {
      const element = e;
      if (element.type === type) {
        element.y -= tileMap.tileHeight;
        result.push(element);
      }
    });
    return result;
  }

  loadLevel() {
    // The map
    // this.map = this.add.tilemap('demoLevel');
    this.map = this.add.tilemap(this.currentLevel);
    // The tilesheet of the map
    // Aqui necesitamos unir el asset del tile asociado al mapa con el asset que hemos cargado.
    // En demoLevel.json esta
    // el nombre del fichero usado, y en preload el de la variable que contiene dicho asset.
    this.map.addTilesetImage('tiles_spritesheet', 'gameTiles');

    // Creamos los layers
    // Se pone el nombre del layer que hemos creado en el programa TILED
    this.backgroundLayer = this.map.createLayer('Background');
    this.collisionLayer = this.map.createLayer('Collision');

    this.game.world.sendToBack(this.backgroundLayer);

    // Lets set the collision layer
    this.map.setCollisionByExclusion([46, 58, 70, 82], true, 'Collision');

    // resize the world to fit the layer
    this.collisionLayer.resizeWorld();

    const goalArray = Game.findObjectByType('goal', this.map, 'Objects');
    this.goal = this.add.sprite(goalArray[0].x, goalArray[0].y, 'goal');
    this.game.physics.arcade.enable(this.goal);
    this.goal.body.allowGravity = false;
    this.goal.nextLevel = goalArray[0].properties.nextLevel;

    // create player
    const playerArray = Game.findObjectByType('player', this.map, 'Objects');
    this.player = this.add.sprite(playerArray[0].x, playerArray[0].y, 'player', 3);
    this.player.anchor.setTo(0.5);
    this.player.animations.add('walking', [0, 1, 2, 1], 6, true);
    this.game.physics.arcade.enable(this.player);
    this.player.customParams = {};
    this.player.body.collideWorldBounds = true;

    this.game.camera.follow(this.player);

    this.createEnemies();
  }

  createEnemies() {
    this.enemies = this.add.group();
    const enemiesArray = Game.findObjectByType('enemy', this.map, 'Objects');
    enemiesArray.forEach((enemy) => {
      this.enemies.add(new Enemy({
        game: this.game,
        x: enemy.x,
        y: enemy.y,
        key: 'slime',
        tilemap: this.map,
        velocity: enemy.properties.velocity
      }));
    });
  }

  createOnScreenControls() {
    this.leftArrow = this.add.button(20, this.game.height - 60, 'arrowButton');
    this.rightArrow = this.add.button(110, this.game.height - 60, 'arrowButton');
    this.actionButton = this.add.button(this.game.width - 100, this.game.height - 60, 'actionButton');

    this.leftArrow.alpha = 0.5;
    this.rightArrow.alpha = 0.5;
    this.actionButton.alpha = 0.5;

    this.leftArrow.fixedToCamera = true;
    this.rightArrow.fixedToCamera = true;
    this.actionButton.fixedToCamera = true;

    this.actionButton.events.onInputDown.add(() => {
      this.player.customParams.mustJump = true;
    });

    this.actionButton.events.onInputUp.add(() => {
      this.player.customParams.mustJump = false;
    });

    this.leftArrow.events.onInputDown.add(() => {
      this.player.customParams.isMovingLeft = true;
    });

    this.leftArrow.events.onInputUp.add(() => {
      this.player.customParams.isMovingLeft = false;
    });

    this.leftArrow.events.onInputOver.add(() => {
      this.player.customParams.isMovingLeft = true;
    });

    this.leftArrow.events.onInputOut.add(() => {
      this.player.customParams.isMovingLeft = false;
    });

    this.rightArrow.events.onInputDown.add(() => {
      this.player.customParams.isMovingRight = true;
    });

    this.rightArrow.events.onInputUp.add(() => {
      this.player.customParams.isMovingRight = false;
    });

    this.rightArrow.events.onInputOver.add(() => {
      this.player.customParams.isMovingRight = true;
    });

    this.rightArrow.events.onInputOut.add(() => {
      this.player.customParams.isMovingRight = false;
    });
  }
}
