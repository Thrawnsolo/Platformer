import phaser from 'phaser';
import goal from '../../assets/images/goal.png';
import slime from '../../assets/images/slime.png';
import player from '../../assets/images/player_spritesheet.png';
import arrowButton from '../../assets/images/arrowButton.png';
import actionButton from '../../assets/images/actionButton.png';
import gameTiles from '../../assets/images/tiles_spritesheet.png';
// import demoLevel from '../../assets/levels/platformer.json';
import level1 from '../../assets/levels/level1.json';
import level2 from '../../assets/levels/level2.json';

export default class Preload extends phaser.State {
  preload() {
    this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'preloadBar');
    this.preloadBar.anchor.setTo(0.5);
    this.preloadBar.scale.setTo(3);
    this.load.setPreloadSprite(this.preloadBar);
    this.load.image('goal', goal);
    this.load.image('slime', slime);
    this.load.spritesheet('player', player, 28, 30, 5, 1, 1);
    this.load.image('arrowButton', arrowButton);
    this.load.image('actionButton', actionButton);
    this.load.image('gameTiles', gameTiles);
    // Hack in order to use the json with webpack,
    // I load it and the stringify and pass it as the data argument
    // this.load.tilemap('demoLevel', null, JSON.stringify(demoLevel), phaser.Tilemap.TILED_JSON);
    this.load.tilemap('level1', null, JSON.stringify(level1), phaser.Tilemap.TILED_JSON);
    this.load.tilemap('level2', null, JSON.stringify(level2), phaser.Tilemap.TILED_JSON);
  }
  create() {
    this.state.start('Game');
  }
}
