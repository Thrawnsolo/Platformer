import pixi from 'pixi';
import p2 from 'p2';
import phaser from 'phaser';
import bootState from './states/bootState';
import preloadState from './states/preloadState';
import gameState from './states/gameState';

class Main extends phaser.Game {
  static getDimensions(maxWidth, maxHeight) {
    const width = window.innerWidth * window.devicePixelRatio;
    const height = window.innerHeight * window.devicePixelRatio;
    let landscapeWidth = Math.max(width, height);
    let landscapeHeight = Math.min(width, height);

    if (landscapeWidth > maxWidth) {
      const ratioWidth = maxWidth / landscapeWidth;
      landscapeWidth *= ratioWidth;
      landscapeHeight *= ratioWidth;
    }
    if (landscapeHeight > maxHeight) {
      const ratioHeight = maxHeight / landscapeHeight;
      landscapeWidth *= ratioHeight;
      landscapeHeight *= ratioHeight;
    }
    return { width: landscapeWidth, height: landscapeHeight };
  }

  constructor() {
    const dimensions = Main.getDimensions(750, 450);
    super(dimensions.width, dimensions.height, phaser.CANVAS);
    this.state.add('Boot', bootState);
    this.state.add('Preload', preloadState);
    this.state.add('Game', gameState);
  }
}
window.game = new Main();
window.game.state.start('Boot');
