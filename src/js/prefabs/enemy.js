import phaser from 'phaser';

export default class Enemy extends phaser.Sprite {
  constructor(parameters) {
    const {
      game, x, y, key, tilemap
    } = parameters;
    let { velocity } = parameters;
    super(game, x, y, key);

    this.game = game;
    this.tilemap = tilemap;
    this.anchor.setTo(0.5);
    if (typeof velocity === 'undefined') {
      velocity = (40 + (Math.random() * 20)) + (Math.random() < 0.5 ? 1 : -1);
    }

    this.game.physics.arcade.enableBody(this);
    this.body.collideWorldBounds = true;
    this.body.bounce.set(1, 0);
    this.body.velocity.x = velocity;
  }

  update() {
    let direction;
    if (this.body.velocity.x > 0) {
      this.scale.setTo(-1, 1);
      direction = 1;
    } else {
      this.scale.setTo(1);
      direction = -1;
    }
    const nextX = this.x + (direction * (Math.abs(this.width / 2) + 1));
    const nextY = this.bottom + 1;
    const nextTile = this.tilemap.getTileWorldXY(nextX, nextY, this.tilemap.tileWidth, this.tilemap.tileHeight, 'Collision');
    if (!nextTile && this.body.blocked.down) {
      this.body.velocity.x *= -1;
    }
  }
}
